document.addEventListener("DOMContentLoaded", () => {

  const pieces = document.querySelectorAll("#board .piece");
  
  let pieceSelected = false;
  let currentPlayer = "black";
  const squares = document.querySelectorAll("#board .black"); 
  

  
  const uncheckPiece = function() {
    this.classList.remove("selected");
    pieceSelected = false;
    this.removeEventListener("click", uncheckPiece);

  }
  const moveone = function(event) {
    const origin = event.target.origin;
    const destination = event.target.destination;

    for(let x = 0; x < squares.length; x++){
      if(squares[x].children.length === 0){
        squares[x].removeEventListener("click", moveone);
      }
    }

    destination.innerHTML = '<div class="piece piece-' +currentPlayer+'"></div>'; 
    origin.remove();
    pieceSelected = false;

    if(currentPlayer == "black"){
      currentPlayer = "white";
    }else{
      currentPlayer = "black";
    }  
  }

    for(let i = 0; i < pieces.length; i++){
      pieces[i].addEventListener("click", () => {

        if(!pieceSelected && pieces[i].classList.contains("piece-" + currentPlayer)){
          pieces[i].classList.add("selected");
          pieceSelected = true;
		  
        // if(squares[16].children.length === 0){
        //   squares[16].innerHTML = '<div class="piece piece-' +currentPlayer+'"></div>'; 
        //   pieces[i].remove();
        // }
          for(let x = 0; x < squares.length; x++){
            if(squares[x].children.length === 0){
              squares[x].addEventListener("click", moveone);
              squares[x].origin = pieces[i];
              squares[x].destination = squares[x];
            }
          }
        }
       document.querySelector("#board .selected").addEventListener("click", uncheckPiece);
				
      });
    }

});